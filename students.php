<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>UTB PROJECT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link id="callCss" rel="stylesheet" href="themes/bootshop/bootstrap.min.css" media="screen"/>
    <link href="themes/css/base.css" rel="stylesheet" media="screen"/>
<!-- Bootstrap style responsive -->	
	<link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
	<link href="themes/css/font-awesome.css" rel="stylesheet" type="text/css">
<!-- Google-code-prettify -->	
	<link href="themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
<!-- fav and touch icons -->
    <link rel="shortcut icon" href="themes/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="themes/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="themes/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="themes/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="themes/images/ico/apple-touch-icon-57-precomposed.png">
	<style type="text/css" id="enject"></style>
  </head>
<body>
<div id="header">
<div class="container">
<div id="welcomeLine" class="row">
	<div class="span6"></div>
	<div class="span6">
	<div class="pull-right">
		</div>
	</div>
</div>
<!-- Navbar ================================================== -->
<div id="logoArea" class="navbar">
<a id="smallScreen" data-target="#topMenu" data-toggle="collapse" class="btn btn-navbar">
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</a>
  <div class="navbar-inner">
    <a class="brand" href="index.html"><img src="themes/images/logo.png"></a>
		
	<ul id="topMenu" class="nav pull-center">
	<li class=""><a href="#.html"><big><strong><font color="#FFCC33">    UNIVERSTY OF TOURSIM TECHNOLOGY AND BUSINESS STUDIES</font></strong></big></a></li>
	</ul>
    <ul id="topMenu" class="nav pull-right">
	 <li class=""><a href="#.html">ABOUT UTB</a></li>
	 <li class=""><a href="#.html">MYUTB</a></li>
	 <li class=""><a href="#.html">WEBMAIL</a></li>
	 
	 <li class=""><a href="userlogin.php"><span class="badge badge-warning "><i class="icon-user"></i>LogOut</a></span></a></li>
	 
    </ul>
  </div>
</div>
</div>
</div>
<!-- Header End====================================================================== -->
<div id="mainBody">
	<div class="container">
	<div class="row">
<!-- Sidebar ================================================== -->
	<div id="sidebar" class="span3">
		<div class="well well-small"><a id="myCart" href="product_summary.html">DEPARTMENT  <span class="badge badge-success pull-right"><i class="icon-star-empty"></i>UTB <i class="icon-star-empty"></i></span></a></div>
		<ul id="sideManu" class="nav nav-tabs nav-stacked">
			<li class="subMenu open"><a> INFORMATION TECHNOLOGY</a>
				<ul>
				</ul>
			</li>
			<li class="subMenu"><a> BUSINESS ADMINISTRATION </a>
			<ul style="display:none">												
			</ul>
			</li>
			<li class="subMenu"><a>HOTEL MANAGEMENT</a>
				<ul style="display:none">											
			</ul>
			</li>
						</li>
			<li class="subMenu"><a>TOURSME MANAGEMENT</a>
				<ul style="display:none">												
			</ul>
			</li>
		</ul>
         <br/>
	</div>
<!-- Sidebar end=============================================== -->
		<div class="span9">	
		    <ul class="breadcrumb">
		    <li><a href="index.php"><i class="icon-home"></i>Home</a> <span class="divider">/</span></li>
    <li class="active"><i class="icon-pencil"></i>Students</a> <span class="divider">/</span></li>
			
				
            </li>
                      <ul class="nav pull-right">
						  <li><a href="students.php"><font color="#1132F7"><i class="icon-refresh title="></i>refresh</font></a></li></ul>
    </ul>	
	<div class="well">
	<form action="students1.php" method="post" class="form-horizontal" >
		<h4>Student registration</h4>
		<div class="control-group">
			<label class="control-label">reg n<sup>o</sup></label>
			<div class="controls">
			  <input type="number" name="std_regno" placeholder="registration number">
			</div>
		 </div>
		<div class="control-group">
			<label class="control-label">First name </label>
			<div class="controls">
			  <input type="text" name="First_name" placeholder="First Name" required>
			</div>
		 </div>
		 <div class="control-group">
			<label class="control-label">Last name</label>
			<div class="controls">
			  <input type="text" name="Last_name" placeholder="Last Name" required>
			</div>
		 </div>
  	  <div class="control-group">
			<label class="control-label">Department</label>
			<div class="controls">
			<select name="program">
			  	<option>INFORMATION TECHNOLOGY</option>
			  	<option>BUSINESS ADMINISTRATION</option>
			  	<option>HOTEL MANAGEMENT</option>
			  	<option>TOURSME MANAGEMENT</option>
				</select>
			 </div>
			 </div>
	  	  <div class
	  	  <div class="control-group">
		<label class="control-label">Module </label>
		<div class="controls">
					<select class="srchTxt" name="moduleid">
					<option>---select any course---</option>
 <?PHP
 $con=mysql_connect("localhost","root","");
  mysql_select_db("utb_db",$con);
   $query=mysql_query("select * from module",$con);
   while($a=mysql_fetch_array($query))
   {
   ?>     
<option value="<?php echo $a['lectureid']; ?>"><?php echo"".$a['module_name']."";?></option>
			
<?php
}
?>
</select>
		</div>
	  </div>	  <br>
	
	<div class="control-group">
			<div class="controls">
				<input type="hidden" name="email_create" value="1">
				<input type="hidden" name="is_new_customer" value="1">
				<input class="btn btn-large btn-success" type="submit" value="Register" />
			</div>
		</div>	
	</form>
</div>

</div>
</div>
</div>
</div>
<!-- Footer ================================================================== -->
	<div  id="footerSection">

		<p align="center">Copy right &copy; 2017 all right reserved 2017 <br><big>University of Tourism, Technology and Business Studies <strong>(UTB)</strong></big> </p>

	</div>
<!-- Placed at the end of the document so the pages load faster ============================================= -->
	<script src="themes/js/jquery.js" type="text/javascript"></script>
	<script src="themes/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="themes/js/google-code-prettify/prettify.js"></script>
	
	<script src="themes/js/bootshop.js"></script>
    <script src="themes/js/jquery.lightbox-0.5.js"></script>
	
	<!-- Themes switcher section ============================================================================================= -->

		 
	</div>
	</div>
</div>
<span id="themesBtn"></span>
</body>
</html>