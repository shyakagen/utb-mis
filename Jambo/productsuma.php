<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>


<table class="table table-bordered" border="1">
<thead>
<tr>
<th>Product</th>
<th>Description</th>
<th>Quantity/Update</th>
<th>Price</th>
<th>Discount</th>
<th>Tax</th>
<th>Total</th>
</tr>
</thead>
<tbody>
<tr>
<td> <img width="60" src="themes/images/products/4.jpg" alt=""/></td>
<td>MASSA AST<br/>Color : black, Material : metal</td>
<td>
<div class="input-append"><input class="span1" style="max-width:34px" placeholder="1" id="appendedInputButtons" size="16" type="text"><button class="btn" type="button"><i class="icon-minus"></i></button><button class="btn" type="button"><i class="icon-plus"></i></button><button class="btn btn-danger" type="button"><i class="icon-remove icon-white"></i></button>	</div>
</td>
<td>$120.00</td>
<td>$25.00</td>
<td>$15.00</td>
<td>$110.00</td>
</tr>
<tr>
<td> <img width="60" src="themes/images/products/8.jpg" alt=""/></td>
<td>MASSA AST<br/>Color : black, Material : metal</td>
<td>
<div class="input-append"><input class="span1" style="max-width:34px" placeholder="1" size="16" type="text"><button class="btn" type="button"><i class="icon-minus"></i></button><button class="btn" type="button"><i class="icon-plus"></i></button><button class="btn btn-danger" type="button"><i class="icon-remove icon-white"></i></button>	</div>
</td>
<td>$7.00</td>
<td>--</td>
<td>$1.00</td>
<td>$8.00</td>
</tr>
<tr>
<td> <img width="60" src="themes/images/products/3.jpg" alt=""/></td>
<td>MASSA AST<br/>Color : black, Material : metal</td>
<td>
<div class="input-append"><input class="span1" style="max-width:34px" placeholder="1" size="16" type="text"><button class="btn" type="button"><i class="icon-minus"></i></button><button class="btn" type="button"><i class="icon-plus"></i></button><button class="btn btn-danger" type="button"><i class="icon-remove icon-white"></i></button>	</div>
</td>
<td>$120.00</td>
<td>$25.00</td>
<td>$15.00</td>
<td>$110.00</td>
</tr>

<tr>
<td colspan="6" style="text-align:right">Total Price:	</td>
<td> $228.00</td>
</tr>
<tr>
<td colspan="6" style="text-align:right">Total Discount:	</td>
<td> $50.00</td>
</tr>
<tr>
<td colspan="6" style="text-align:right">Total Tax:	</td>
<td> $31.00</td>
</tr>
<tr>
<td colspan="6" style="text-align:right"><strong>TOTAL ($228 - $50 + $31) =</strong></td>
<td class="label label-important" style="display:block"> <strong> $155.00 </strong></td>
</tr>
</tbody>
</table> 

</body>
</html>